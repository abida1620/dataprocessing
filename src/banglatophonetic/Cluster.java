/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banglatophonetic;

import java.util.ArrayList;

/**
 *
 * @author tishpish
 */
public class Cluster
{
    int id;
    ArrayList<News> allNews ;
    ArrayList<TagNode> allTags;
    public Cluster(int id)
    {
        this.id = id;
        allNews = new ArrayList<News>();
        allTags = new ArrayList<TagNode>();
    }
    
    void addNews(News news)
    {
        if (!allNews.contains(news))
        {
            allNews.add(news);
            for (int i=0;i<news.tagNodeList.size();i++)
            {
                TagNode tNode = news.tagNodeList.get(i);
                allTags.add(tNode);
            }
        }
    }
    void addTag (TagNode tag)
    {
        if (!allTags.contains(tag))
        {
            allTags.add(tag);
        }
    }
    
    void printCluster(int idd)
    {
        System.out.println("id: "+idd);
        for (int i=0;i<allNews.size();i++)
        {
            News news = allNews.get(i);
            System.out.println(news.title);
        }
        
        System.out.println("-----------------");
    }
    
    boolean mergeClusters(Cluster ref)
    {
        int matchNewsCount =0;
        for (int i=0;i<allNews.size();i++)
        {
            News tag = allNews.get(i);
            for (int j=0;j<ref.allNews.size();j++)
            {
                News refTag = ref.allNews.get(j);
                if (refTag == tag)
                {
                    matchNewsCount++;
                    break;
                }
            }
        }
        
        /*int matchTagCount =0;
        int totAvailTag=0;
        
        for (int i=0;i<allTags.size();i++)
        {
            TagNode tag = allTags.get(i);
            for (int j=0;j<ref.allTags.size();j++)
            {
                TagNode refTag = ref.allTags.get(j);
                if (refTag == tag)
                {
                    matchTagCount++;
                    break;
                }
            }
            totAvailTag +=ref.allTags.size();
        }
        
        double tagPercent = (double)matchTagCount/totAvailTag;*/
        
        double matchPercent = ((double)matchNewsCount/ref.allNews.size());
        double matchPercent2 = ((double)matchNewsCount/allNews.size());
        if (matchPercent>0.65 || matchPercent2>0.65)
        {
            for (int j=0;j<ref.allNews.size();j++)
            {
                News refNews = ref.allNews.get(j);
                addNews(refNews);
                
            }
            return true;
        }
        
        return false; // true means merged
    }
    
    
}
