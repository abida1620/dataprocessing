/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banglatophonetic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author tishpish
 */
class NonTagTree
{
    private NonTagNode root;
    public NonTagTree() throws FileNotFoundException
    {
       root =  creatTree();
    }
    
    NonTagNode creatTree() throws FileNotFoundException
    {
        FileReader fp = new FileReader("nontag");
        Scanner sc = new Scanner(fp);
        
        ArrayList<String> allTag = new ArrayList<>();
        
        while (sc.hasNext())
        {
            
            String line = sc.next();
            //Word word = new Word(line);
            if (!allTag.contains(line))
                    allTag.add(line);
            
        }
        
        Collections.sort(allTag);
        
        NonTagNode root = new NonTagNode('-', false);
        
        for (int i=0;i<allTag.size();i++)
        {
            //System.out.println("-------");
            root.insert(allTag.get(i));
        }
        System.out.println("Non Tag Tree Created");
        return root;
    }

    public NonTagNode getRoot()
    {
        return root;
    }
    
    
    
    
    
}
