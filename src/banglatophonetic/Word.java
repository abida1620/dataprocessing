/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banglatophonetic;

import java.util.Collections;

/**
 *
 * @author tishpish
 */
public class Word
{
    String main,translated,processed;
    public Word(String line)
    {
        this.main = line;
        this.translated= getEnglish(line);
        this.processed = getProcessed(line);
    }

    private String getEnglish(String line)
    {
            String res="";
            char c[] = line.toCharArray();
            for (int i=0;i<c.length;i++)
            {
                if (c[i]=='ক')
                {
                    res+="ko";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='খ')
                {
                    res+="kho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='গ')
                {
                    res+="go";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ঘ')
                {
                    res+="gho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ঙ')
                {
                    res+="Ng";
                }
                else if (c[i]=='চ')
                {
                    res+="co";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ছ')
                {
                    res+="cho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='জ')
                {
                    res+="jo";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ঝ')
                {
                    res+="jho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ঞ')
                {
                    res+="NG";
                }
                else if (c[i]=='ট')
                {
                    res+="To";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ঠ')
                {
                    res+="Tho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ড')
                {
                    res+="Do";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ঢ')
                {
                    res+="Dho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ণ')
                {
                    res+="No";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ত')
                {
                    res+="to";
                    
                }
                else if (c[i]=='থ')
                {
                    res+="tho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='দ')
                {
                    res+="do";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ধ')
                {
                    res+="dho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                 else if (c[i]=='ন')
                 {
                    res+="no";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                 }
                else if (c[i]=='প')
                {
                    res+="po";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ফ')
                {
                    res+="fo";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ব')
                {
                    res+="bo";
                    
                }
                else if (c[i]=='ভ')
                {
                    res+="vo";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ম')
                {
                    res+="mo";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                
                else if (c[i]=='য')
                {
                    res+="zo";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='র')
                {
                    res+="ro";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ল')
                {
                    res+="lo";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='শ')
                {
                    res+="sho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ষ')
                {
                    res+="Sho";
                    
                }
                else if (c[i]=='স')
                {
                    res+="so";
                    
                }
                else if (c[i]=='হ')
                {
                    res+="ho";
                    
                }
                else if (c[i]=='ড়')
                {
                    res+="Ro";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='ঢ়')
                {
                    res+="Rho";
                    if (i+1 == c.length)
                    {
                        res=res.substring(0, res.length()-1);
                    }
                }
                else if (c[i]=='য়')
                {
                    res+="y";
                }
                else if (c[i]=='ৎ')
                {
                    res+="TH";
                }
                else if (c[i]=='ং')
                {
                    res+="ng";
                }
                else if (c[i]=='ঃ')
                {
                    res+="uh";
                }
                //ঁ
                else if (c[i]=='ঁ')
                {
                    res+="";
                }
                else if (c[i]=='অ')
                {
                    res+="o";
                }
                else if (c[i]=='আ')
                {
                    res+="a";
                }
                else if (c[i]=='ই')
                {
                    res+="i";
                }
                else if (c[i]=='ঈ')
                {
                    res+="I";
                }
                else if (c[i]=='উ')
                {
                    res+="u";
                }
                else if (c[i]=='ঊ')
                {
                    res+="U";
                }
                else if (c[i]=='ঋ')
                {
                    res+="rri";
                }
                else if (c[i]=='এ')
                {
                    res+="e";
                }
                
                else if (c[i]=='ঐ')
                {
                    res+="OI";
                }
                
                else if (c[i]=='ও')
                {
                    res+="O";
                }
                else if (c[i]=='ঔ')
                {
                    res+="OU";
                }
               
                
                 else if (c[i]=='া')
                 {
                        res=res.substring(0, res.length()-1);
                        res+="a";
                 }
                 else if (c[i]=='ে')
                 {
                      res=res.substring(0, res.length()-1);
                     res+="e";
                 }
                 else if (c[i]=='ৈ')
                 {
                      res=res.substring(0, res.length()-1);
                     res+="OI";
                 }
                 
                else if (c[i]=='ি')
                {
                     res=res.substring(0, res.length()-1);
                    res+="i";
                }
                else if (c[i]=='ী')
                {
                     res=res.substring(0, res.length()-1);
                    res+="I";
                }
                else if (c[i]=='ু')
                {
                     res=res.substring(0, res.length()-1);
                    res+="u";
                }
                    
                else if (c[i]=='ূ')
                {
                    res=res.substring(0, res.length()-1);
                    res+="U";
                }
                else if (c[i]=='ো')
                {
                    res=res.substring(0, res.length()-1);
                    res+="O";
                }
                else if (c[i]=='ৌ')
                { 
                    res=res.substring(0, res.length()-1);
                    res+="U";
                }
                else if (c[i]=='্')
                {
                     res=res.substring(0, res.length()-1);
                     
                }
                //ৃ
                else if (c[i]=='ৃ')
                { 
                    res=res.substring(0, res.length()-1);
                    res+="rri";
                }
                //
                else
                {
                    res+=c[i]+"";
                }
                   
                
                
               
                ///System.out.println(res++" "+ c[i]);
                
            }
            return res;
    }
    
    String getProcessed(String line)
    {
        line = getReverse(line);
        /*line = line.replaceFirst("রেক", "");
        line = line.replaceFirst("রেদ", "");//ের
        line = line.replaceFirst("রেয়", "");
        line = line.replaceFirst("রে", "ে");
        line = line.replaceFirst("য়ী", "");
        line = line.replaceFirst(getReverse("তে"), "ত");
        line = line.replaceFirst(getReverse("বে"), "ব");
        line = line.replaceFirst(getReverse("রে"), "র");
        line = line.replaceFirst(getReverse("কে"), "");//ায়
        line = line.replaceFirst(getReverse("ায়"), "া");*/
        return getReverse(line);
    }
    
    String getReverse(String line)
    {
        String x="";
        for (int i=0;i<line.length();i++)
        {
            x=line.charAt(i)+x;
        }
        return x;
    }
    
}
