/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banglatophonetic;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author tishpish
 */
class TagNode
{
    char c;
    boolean ending;
    ArrayList<News> allNews;
    ArrayList<TagNode> childs;
    
    public TagNode(char c,boolean ending)
    {
        this.c = c;
        this.ending=ending;
        childs = new ArrayList<TagNode>();
        allNews = new ArrayList<News>();
    }
    
    public void insert(String line,News news,String prev)
    {
        if (line.length()<=0)
            return;
        
        char cur = line.charAt(0);
        prev+=cur;
        
        if (isAvailable(cur))
        {
            if (line.length()==1)
            {
                TagNode found = getMatchedTagChar(cur);
                //found.insert(line.substring(1),news); 
                found.allNews.add(news);
                news.tagList.add(prev);
                news.tagNodeList.add(found);
            }
            else
            {
                TagNode found = getMatchedTagChar(cur);
                if (found.ending && (matchThreshold(line, prev)) )
                {
                    found.allNews.add(news);
                    news.tagList.add(prev);
                    news.tagNodeList.add(found);
                    return;
                }
                else
                found.insert(line.substring(1),news,prev); 
            }
            
        }
        else
        {
            if (line.length()==1)
            {
                //System.out.println("last node: "+cur);
                TagNode newTag = new TagNode(cur, true);
                newTag.allNews.add(news);
                childs.add(newTag);
                news.tagList.add(prev);
                news.tagNodeList.add(newTag);
                return;
            }
            else
            {
               // System.out.println("new node: "+cur);
                TagNode newTag = new TagNode(cur, false);
                childs.add(newTag);
                newTag.insert(line.substring(1),news,prev);
            }
        }
        
    }
    public boolean matchThreshold(String line, String prev)
    {
        double val = ((double) line.length())/(prev.length()+line.length()-1);
        if (line.length()-1>prev.length())
            return false;
        else
            return true;
    }
            
    public void insert(String line)
    {
        if (line.length()<=0)
            return;
        char cur = line.charAt(0);
        if (isAvailable(cur))
        {
            TagNode found = getMatchedTagChar(cur);
            found.insert(line.substring(1));
        }
        else
        {
            if (line.length()==1)
            {
                //System.out.println("last node: "+cur);
                TagNode newTag = new TagNode(cur, true);
                childs.add(newTag);
                return;
            }
            else
            {
               // System.out.println("new node: "+cur);
                TagNode newTag = new TagNode(cur, false);
                childs.add(newTag);
                newTag.insert(line.substring(1));
            }
        }
    }
    
    
    
    
    boolean isAvailable(char toFind)
    {
        
        for (int i=0;i<childs.size();i++)
        {
            TagNode cx = childs.get(i);
            if (cx.c == toFind)
            {
                return true;
            }
        }
        return false;
    }

    private TagNode getMatchedTagChar(char toFind)
    {
        for (int i=0;i<childs.size();i++)
        {
            TagNode cx = childs.get(i);
            if (cx.c == toFind)
            {
                return cx;
            }
        }
        return null;
        
    }
    
    public void printAllTags(String prev)
    {
        String nowVal = prev+c;
        if (ending  )
        {
            //System.out.println(nowVal.substring(1));
            System.out.println(nowVal.substring(1)/*+"("+allNews.size()+")"*/);
            if (allNews.size()==0)
            {
                //System.out.println("-------------------");
            }
            for (int i=0;i<allNews.size();i++)
            {
                News news = allNews.get(i);
                //System.out.println(news.title);
            }
        }
        
        for (int i=0;i<childs.size();i++)
        {
            TagNode cx = childs.get(i);
            cx.printAllTags(nowVal);
        }
        return;
    }
    
    
    boolean find (String line)
    {
        if (line.length()==0)
        {
            return ending;
        }
            
        
        char cur = line.charAt(0);
        if (isAvailable(cur))
        {
            //System.out.println("Allready available node: "+cur+"   line:"+line);
            
            TagNode found = getMatchedTagChar(cur);
            return found.find(line.substring(1));
        }
        else
            return false;
    }
    
}
