/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banglatophonetic;

import java.util.ArrayList;

/**
 *
 * @author tishpish
 */
public class NonTagNode
{
    char c;
    boolean ending;
    
    ArrayList<NonTagNode> childs;
    
    public NonTagNode(char c,boolean ending)
    {
        this.c = c;
        this.ending=ending;
        childs = new ArrayList<NonTagNode>();
    }
    
    public void insert(String line)
    {
        if (line.length()<=0)
            return;
        char cur = line.charAt(0);
        if (isAvailable(cur))
        {
            //System.out.println("Allready available node: "+cur);
            NonTagNode found = getMatchedTagChar(cur);
            found.insert(line.substring(1));
        }
        else
        {
            if (line.length()==1)
            {
                //System.out.println("last node: "+cur);
                NonTagNode newTag = new NonTagNode(cur, true);
                childs.add(newTag);
                return;
            }
            else
            {
               // System.out.println("new node: "+cur);
                NonTagNode newTag = new NonTagNode(cur, false);
                childs.add(newTag);
                newTag.insert(line.substring(1));
            }
        }
    }
    
    boolean isAvailable(char toFind)
    {
        
        for (int i=0;i<childs.size();i++)
        {
            NonTagNode cx = childs.get(i);
            if (cx.c == toFind)
            {
                return true;
            }
        }
        return false;
    }

    private NonTagNode getMatchedTagChar(char toFind)
    {
        for (int i=0;i<childs.size();i++)
        {
            NonTagNode cx = childs.get(i);
            if (cx.c == toFind)
            {
                return cx;
            }
        }
        return null;
        
    }
    
    public void printAllTags(String prev)
    {
        String nowVal = prev+c;
        if (ending)
            System.out.println(nowVal);
        
        for (int i=0;i<childs.size();i++)
        {
            NonTagNode cx = childs.get(i);
            cx.printAllTags(nowVal);
        }
        return;
    }
    
    
    boolean find (String line)
    {
        if (line.length()==0)
        {
            return ending;
        }
        
        char cur = line.charAt(0);
        if (isAvailable(cur))
        {
            NonTagNode found = getMatchedTagChar(cur);
            return found.find(line.substring(1));
        }
        else
            return false;
    }
    
    
    
}
