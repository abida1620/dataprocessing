/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banglatophonetic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**ার
 *
 * @author tishpish
 */
public class TagTree
{
    private TagNode root;
    public TagTree() throws FileNotFoundException
    {
       root =  creatTree();
    }
    
    TagNode creatTree() throws FileNotFoundException
    {
        ArrayList<String> allTag = new ArrayList<>();
        //TagNode root = new TagNode('-', false);
        
        FileReader fp = new FileReader("tag");
        Scanner sc = new Scanner(fp);
        
        
        while (sc.hasNext())
        {
            
            String line = sc.next();
            //Word word = new Word(line);
            if (!allTag.contains(line))
                    allTag.add(line);
            
        }
        
        Collections.sort(allTag);
        
        TagNode root = new TagNode('-', false);
        
        for (int i=0;i<allTag.size();i++)
        {
            //System.out.println("-------");
            root.insert(allTag.get(i));
        }
        System.out.println("Tag Tree Created");
        return root;
        
    }
    
    
    TagNode insertNewTags(ArrayList<News> allNews,NonTagNode nonTagRoot)
    {
        //ArrayList<String> allTag = new ArrayList<>();
        //TagNode root = new TagNode('-', false);
        
        for (int i=0;i<allNews.size();i++)
        {
            
            News news = allNews.get(i);
            news.addTags(nonTagRoot,root);
            
            /*for (int j=0;j<news.tagList.size();j++)
            {
                String string = news.tagList.get(j);
                root.insert(string,news);
            }*/
            
            
        }
        
        System.out.println("Non Tag Tree Created");
        return root;
    }

    public TagNode getRoot()
    {
        return root;
    }
    
}
