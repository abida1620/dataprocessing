/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banglatophonetic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
/**
 *
 * @author tishpish
 */
public class BanglaToPhonetic
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException
    {
        // TODO code application logic here
        Scanner user = new Scanner(System.in);
        NonTagTree nonTagTree = new NonTagTree();
        NonTagNode root = nonTagTree.getRoot();
        
        //root.printAllTags("");
        
        FileReader fp = new FileReader("titles2.txt");
        Scanner sc = new Scanner(fp);
        ArrayList<News> allNews = new ArrayList<News>();
        while (sc.hasNextLine())
        {
            News news = new News("100", sc.nextLine(), "100");
            //news.addTags(root);
            allNews.add(news);
        }
        
        TagTree tagTree = new TagTree();
        TagNode taggedRoot = tagTree.getRoot();
        
        tagTree.insertNewTags(allNews,root);
        
        //taggedRoot.printAllTags("");
        
        
        
        
        
        
        
        ArrayList<Cluster> clusters = new ArrayList<Cluster>();
        
        for (int i=0;i<allNews.size();i++)
        {
            Cluster newCluster = new Cluster((i));
            
            News ref = allNews.get(i);
            newCluster.addNews(ref);
            
           
            for (int j=0;j<allNews.size();j++)
            {
                News compare = allNews.get(j);
                if (compare!=ref)
                {
                    int count =0;
                    for (int k=0;k<ref.tagNodeList.size() && ref.tagNodeList.size()>=2;k++)
                    {
                        TagNode reftag = ref.tagNodeList.get(k);
                        for (int l=0;l<compare.tagNodeList.size() && compare.tagNodeList.size()>=2;l++)
                        {
                            TagNode comtag = compare.tagNodeList.get(l);
                            if (reftag==comtag) // news can be same
                            {
                                count++;
                                
                            }
                        }
                    }
                    if (count>=3)
                    {
                        newCluster.addNews(compare);
                        
                                //System.out.println(compare.title);
                    }

                }
                
            }
            if (newCluster.allNews.size()>2)
                clusters.add(newCluster);
            //System.out.println("------------------");
        }
        
        System.out.println("First Clustering Complete");
        
        for (int i=0;i<clusters.size();i++)
        {
            Cluster cluster = clusters.get(i);
            for (int j=0;j<clusters.size();j++)
            {
                Cluster refcluster = clusters.get(j);
                if (cluster!=refcluster)
                {
                    if (cluster.mergeClusters(refcluster))
                    {
                        clusters.remove(j);
                        j--;
                    }
                }
            }
            //cluster.printCluster(i);
        }
        
        
        System.out.println("Second Clustering Complete");
        
        for (int i=0;i<clusters.size();i++)
        {
            Cluster cluster = clusters.get(i);
            for (int j=0;j<clusters.size();j++)
            {
                Cluster refcluster = clusters.get(j);
                if (cluster!=refcluster)
                {
                    if (cluster.mergeClusters(refcluster))
                    {
                        clusters.remove(j);
                        j--;
                    }
                }
            }
            cluster.printCluster(i);
        }
        
        
        
        
        
        
        
        /*for (int i=0;i<allNews.size();i++)
        {
            News news = allNews.get(i);
            ArrayList<News> newList = new ArrayList<News>();
            for (int j=0;j<news.tagNodeList.size();j++)
            {
                TagNode tagNode = news.tagNodeList.get(j); // tag of a news
                
                for (int k=0;k<tagNode.allNews.size();k++) // all news under this tag
                {
                    News ok = tagNode.allNews.get(k); // a single news under the tag
                    
                    for (int x=0;x<ok.tagNodeList.size() && ok.tagNodeList.size()>2;x++) // all the tags of the single news
                    {
                        TagNode newTagNode = ok.tagNodeList.get(x); // a single tag of the news
                        if (newTagNode!=tagNode)
                        {
                            for (int t=0;t<newTagNode.allNews.size();t++)
                            {
                                News dekhi = newTagNode.allNews.get(t);
                                if (!newList.contains(dekhi))
                                    newList.add(dekhi);
                            }
                            
                        }
                    }
                    
                    
                    //System.out.println("-------");
                }
            }
            System.out.println("-----------------");
            for (int j=0;j<newList.size();j++)
            {
                News ok = newList.get(j);
                System.out.println(ok.title);
            }
        }*/
        
        
    }
    
}
